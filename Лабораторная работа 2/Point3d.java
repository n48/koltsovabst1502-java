public class Point3d {//  

// ,    	
	private double xCor, yCor, zCor;
	
//  
	public Point3d(){
		xCor=0;
		yCor=0;
		zCor=0;
	}
// 
	public Point3d(double x, double y, double z){
		xCor=x;
		yCor=y;
		zCor=z;
	}
	
//  X
	public double getX(){
		return xCor;
	}
//  Y
	public double getY(){
		return yCor;
	}

//  Z
	public double getZ(){
		return zCor;
	}

//  X
	public void setX(double val){
		xCor = val;
	}
	
//  Y
	public void setY(double val){
		yCor = val;
	}
	
//  Z
	public void setZ(double val){
		zCor = val;
	}
//  ,    	
	public boolean equals(Point3d a){
		if ((a.getX()==xCor) && (a.getY() == yCor) && (a.getZ() == zCor))
			return true;
		return false;
	}
 	
	public double distanceTo(Point3d a){
		return Math.sqrt(Math.pow((a.getX()-xCor), 2) + Math.pow((a.getY()-yCor),2)+Math.pow((a.getZ()-zCor),2));
	}
}