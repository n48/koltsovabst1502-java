import java.util.Scanner;

public class Prime {
	// Происходит проверка всех чисел на отрезке [2;N] и вывод в консоли чисел, которые явлвяются простыми с помощью функции Primes
	public static void searchPrime() {
		int N=scanNumber();
		for (int i=2;i<=N;i++){
			if (Primes(i)) {
				print(i);
			}
		}
	}

	// Функция в качестве входного параметра получает число. Если это число делится на что-то, кроме самого себя, то возвращается false, иначе true.
	public static boolean Primes(int n) {
		for (int i=2;i<n;i++) {
			if (n%i==0) {
				return false;
			}
		}
		return true;
	}

    //Cчитываем с консоли введенное число
	public static int scanNumber() {
		Scanner scanner = new Scanner(System.in);
		return scanner.nextInt();
	}
	
	//Вывод на консоль
	public static void print(int n) {
		System.out.println(n);
	}
}
