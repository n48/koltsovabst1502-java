import java.util.Scanner;

public class Palindrom {
    // На вход получаем массив слов. Строка сохраняется в строку и, с помощью функции ifPalindrom определяем, является ли строка палиндромом.
    public static void searchPalindrom() 
	{
        String N = scanLine();
        if (ifPalindrom(N))
            System.out.println("Palindrom");
        else
            System.out.println("Not palindrom");
    }

    // Функция в качестве входного параметра получает строку, формирует новую строку reverse, посимвольно копирует в обратном порядке из s в reverse и возвращает reverse.
    public static String reverseString(String s) {
        String reverse = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            reverse = reverse + s.charAt(i);
        }
        return reverse;
    }

    // Функция в качестве входного параметра получает строку. Формируется новая строка, обратная изначальной, с помощью функции reverseString. Сравниваем строки и, если функции эквивалентны, возвращаем true, иначе false.
    public static boolean ifPalindrom(String s) {
        String s1 = reverseString(s);
        if (s1.equals(s))//сравнение строк s1 и s
            return true;
        else
            return false;
    }

    //Cчитывание с консоли введенное число
    public static String scanLine() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}

